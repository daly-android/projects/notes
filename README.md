## Notes App

## Overview
This is an application to handle Notes (Create, Update, Delete)

It contains two principle screens, one for notes list and another for each note details.

Each note has a title, content, date and color.

Notes list can be ordered by title, date or color and sorted ascending or descending.


|                                                                                                         |                                                                                                                     |                                                                                                         |
|:-------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------:|
| <img width="1604" alt="notes_list_order_section_hidden" src="docs/notes_list_order_section_hidden.png"> |      <img width="1604" alt="notes_list_order_date_descending" src="docs/notes_list_order_date_descending.png">      | <img width="1604" alt="notes_list_order_date_ascending" src="docs/notes_list_order_date_ascending.png"> |
|          <img width="1604" alt="notes_list_delete_note" src="docs/notes_list_delete_note.png">          | <img width="1604" alt="add_edit_note_save_invalid_title_erro" src="docs/add_edit_note_save_invalid_title_erro.png"> |                   <img width="1604" alt="add_edit_note" src="docs/add_edit_note.png">                   |


## Technical stack :
# 100% Kotlin & Jetpack Compose
# Architecture : MVVM and clean
# Dependency injection : Dagger-Hilt
# Local data persistence : Room
# Build configuration language : Kotlin DSL