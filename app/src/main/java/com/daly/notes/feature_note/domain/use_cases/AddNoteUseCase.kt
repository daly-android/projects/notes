package com.daly.notes.feature_note.domain.use_cases

import com.daly.notes.feature_note.domain.models.InvalidNoteException
import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.repositories.NoteRepository

class AddNoteUseCase(private val noteRepository: NoteRepository) {

    @Throws(InvalidNoteException::class)
    suspend operator fun invoke(note: NoteModel) {
        if (note.title.isBlank()) {
            throw InvalidNoteException("The title of the note can't be empty")
        }
        if (note.content.isBlank()) {
            throw InvalidNoteException("The content of the note can't be empty")
        }
        noteRepository.insertNote(note)
    }
}
