package com.daly.notes.feature_note.presentation.notes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.notes.core.DispatcherService
import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.use_cases.NoteUseCases
import com.daly.notes.feature_note.domain.utils.NoteOrder
import com.daly.notes.feature_note.domain.utils.OrderType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotesViewModel @Inject constructor(
    private val noteUseCases: NoteUseCases,
    private val dispatcherService: DispatcherService
) : ViewModel() {

    private val _state = MutableStateFlow(NotesState())
    val state: StateFlow<NotesState> = _state.stateIn(
        initialValue = NotesState(),
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    private var recentlyDeletedNote: NoteModel? = null

    private var getNotesJob: Job? = null

    init {
        getNotes(NoteOrder.Date(OrderType.Descending))
    }

    fun onEvent(event: NotesEvent) {
        when (event) {
            is NotesEvent.DeleteNote -> {
                viewModelScope.launch(dispatcherService.io) {
                    noteUseCases.deleteNote(event.note)
                    recentlyDeletedNote = event.note
                }
            }

            is NotesEvent.Order -> {
                if ((state.value.noteOrder::class == event.noteOrder::class) &&
                    (state.value.noteOrder.orderType::class == event.noteOrder.orderType::class)
                ) {
                    return
                }
                getNotes(event.noteOrder)
            }

            NotesEvent.RestoreNote -> {
                viewModelScope.launch(dispatcherService.io) {
                    noteUseCases.addNote(recentlyDeletedNote ?: return@launch)
                    recentlyDeletedNote = null
                }
            }

            NotesEvent.ToggleOrderSection -> {
                _state.value = state.value.copy(isOrderSectionVisible = !state.value.isOrderSectionVisible)
            }
        }
    }

    private fun getNotes(noteOrder: NoteOrder) {
        getNotesJob?.cancel()
        getNotesJob = viewModelScope.launch(dispatcherService.io) {
            noteUseCases.getNotes(noteOrder).collect {
                _state.value = state.value.copy(
                    notes = it,
                    noteOrder = noteOrder
                )
            }
        }
    }
}
