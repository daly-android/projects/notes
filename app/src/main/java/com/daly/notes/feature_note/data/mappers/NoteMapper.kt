package com.daly.notes.feature_note.data.mappers

import com.daly.notes.feature_note.data.entities.NoteEntity
import com.daly.notes.feature_note.domain.models.NoteModel

fun NoteEntity.toNoteModel() = NoteModel(
    title = this.title,
    content = this.content,
    timestamp = this.timestamp,
    color = this.color,
    id = this.id
)

fun List<NoteEntity>.toNoteModels() = this.map { it.toNoteModel() }

fun NoteModel.toNoteEntity() = NoteEntity(
    title = this.title,
    content = this.content,
    timestamp = this.timestamp,
    color = this.color,
    id = this.id
)
