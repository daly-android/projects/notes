package com.daly.notes.feature_note.domain.models

data class NoteModel(
    val title: String,
    val content: String,
    val timestamp: Long,
    val color: Int,
    val id: Int? = null
)

class InvalidNoteException(message: String) : Exception(message)
