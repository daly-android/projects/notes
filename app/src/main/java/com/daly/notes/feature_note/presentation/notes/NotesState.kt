package com.daly.notes.feature_note.presentation.notes

import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.utils.NoteOrder
import com.daly.notes.feature_note.domain.utils.OrderType

data class NotesState(
    val notes: List<NoteModel> = emptyList(),
    val noteOrder: NoteOrder = NoteOrder.Date(OrderType.Descending),
    val isOrderSectionVisible: Boolean = false
)
