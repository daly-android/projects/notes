package com.daly.notes.feature_note.domain.use_cases

import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.repositories.NoteRepository

class GetNoteUseCase(private val noteRepository: NoteRepository) {

    suspend operator fun invoke(id: Int): NoteModel? {
        return noteRepository.getNoteById(id)
    }
}
