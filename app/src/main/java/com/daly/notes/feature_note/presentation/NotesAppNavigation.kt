package com.daly.notes.feature_note.presentation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.daly.notes.feature_note.presentation.add_edit_note.AddEditNoteScreen
import com.daly.notes.feature_note.presentation.notes.NotesScreen

const val NOTE_ID_ARGUMENT = "noteId"
const val NOTE_COLOR_ARGUMENT = "noteColor"

@Composable
fun NotesAppNavigation(
    navHostController: NavHostController
) {
    NavHost(
        navController = navHostController,
        startDestination = Screen.NotesScreen.route
    ) {
        composable(route = Screen.NotesScreen.route) {
            NotesScreen(navController = navHostController)
        }

        composable(
            route = Screen.AddEditNoteScreen.route + "?$NOTE_ID_ARGUMENT={$NOTE_ID_ARGUMENT}&$NOTE_COLOR_ARGUMENT={$NOTE_COLOR_ARGUMENT}",
            arguments = (
                listOf(
                    navArgument(
                        name = NOTE_ID_ARGUMENT
                    ) {
                        type = NavType.IntType
                        defaultValue = -1
                    },
                    navArgument(
                        name = NOTE_COLOR_ARGUMENT
                    ) {
                        type = NavType.IntType
                        defaultValue = -1
                    }
                )
                )
        ) {
            val color = it.arguments?.getInt(NOTE_COLOR_ARGUMENT) ?: -1
            AddEditNoteScreen(
                navController = navHostController,
                noteColor = color
            )
        }
    }
}
