package com.daly.notes.feature_note.presentation.add_edit_note

import androidx.compose.ui.graphics.toArgb
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.notes.R
import com.daly.notes.core.DispatcherService
import com.daly.notes.core.ResourcesRepository
import com.daly.notes.feature_note.domain.models.InvalidNoteException
import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.use_cases.NoteUseCases
import com.daly.notes.feature_note.presentation.NOTE_ID_ARGUMENT
import com.daly.notes.feature_note.presentation.utils.noteColors
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class AddEditNoteViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val noteUseCases: NoteUseCases,
    private val resourcesRepository: ResourcesRepository,
    private val dispatcherService: DispatcherService
) : ViewModel() {

    private val initialTitleState = NoteTextFieldState(hint = resourcesRepository.fetchString(R.string.add_edit_note_title_hint))
    private val _noteTitle = MutableStateFlow(initialTitleState)
    val noteTitle: StateFlow<NoteTextFieldState> = _noteTitle.stateIn(
        initialValue = initialTitleState,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    private val initialContentState = NoteTextFieldState(hint = resourcesRepository.fetchString(R.string.add_edit_note_content_hint))
    private val _noteContent = MutableStateFlow(initialContentState)
    val noteContent: StateFlow<NoteTextFieldState> = _noteContent.stateIn(
        initialValue = initialContentState,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    private val initialColorState = noteColors.random().toArgb()
    private val _noteColor = MutableStateFlow(initialColorState)
    val noteColor: StateFlow<Int> = _noteColor.stateIn(
        initialValue = initialColorState,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var currentNoteId: Int? = null

    init {
        savedStateHandle.get<Int>(NOTE_ID_ARGUMENT)?.let { noteId ->
            if (noteId != -1) {
                viewModelScope.launch(dispatcherService.io) {
                    noteUseCases.getNote(id = noteId)?.also { note ->
                        withContext(dispatcherService.main) {
                            currentNoteId = note.id
                            _noteTitle.value = noteTitle.value.copy(
                                text = note.title,
                                isHintVisible = false
                            )
                            _noteContent.value = noteContent.value.copy(
                                text = note.content,
                                isHintVisible = false
                            )
                            _noteColor.value = note.color
                        }
                    }
                }
            }
        }
    }

    fun onEvent(event: AddEditNoteEvent) {
        when (event) {
            is AddEditNoteEvent.ChangeColor -> {
                _noteColor.value = event.color
            }
            is AddEditNoteEvent.ChangeContentFocus -> {
                _noteContent.value = noteContent.value.copy(isHintVisible = !event.focusState.isFocused && noteContent.value.text.isBlank())
            }
            is AddEditNoteEvent.ChangeTitleFocus -> {
                _noteTitle.value = noteTitle.value.copy(isHintVisible = !event.focusState.isFocused && noteTitle.value.text.isBlank())
            }
            is AddEditNoteEvent.EnteredContent -> {
                _noteContent.value = noteContent.value.copy(text = event.value)
            }
            is AddEditNoteEvent.EnteredTitle -> {
                _noteTitle.value = noteTitle.value.copy(text = event.value)
            }
            AddEditNoteEvent.SaveNote -> {
                viewModelScope.launch(dispatcherService.io) {
                    try {
                        noteUseCases.addNote(
                            NoteModel(
                                title = noteTitle.value.text,
                                content = noteContent.value.text,
                                timestamp = System.currentTimeMillis(),
                                color = noteColor.value,
                                id = currentNoteId
                            )
                        )
                        _eventFlow.emit(UiEvent.SaveNote)
                    } catch (e: InvalidNoteException) {
                        _eventFlow.emit(UiEvent.ShowSnackbar(message = e.message ?: resourcesRepository.fetchString(R.string.add_edit_note_save_note_error)))
                    }
                }
            }
        }
    }

    sealed interface UiEvent {
        data class ShowSnackbar(val message: String) : UiEvent
        data object SaveNote : UiEvent
    }
}
