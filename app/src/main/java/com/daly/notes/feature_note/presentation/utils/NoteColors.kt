package com.daly.notes.feature_note.presentation.utils

import androidx.compose.ui.graphics.Color

val noteColors = listOf(
    Color(0xFFD98060),
    Color(0xFFC2B280),
    Color(0xFFFF8DA1),
    Color(0xFF407DD5),
    Color(0xFFBADA55)
)
