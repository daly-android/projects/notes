package com.daly.notes.feature_note.presentation.notes

import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.utils.NoteOrder

sealed interface NotesEvent {
    data class Order(val noteOrder: NoteOrder) : NotesEvent
    data class DeleteNote(val note: NoteModel) : NotesEvent
    data object RestoreNote : NotesEvent
    data object ToggleOrderSection : NotesEvent
}
