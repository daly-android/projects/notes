package com.daly.notes.feature_note.data.repositories

import com.daly.notes.feature_note.data.data_source.NoteDao
import com.daly.notes.feature_note.data.mappers.toNoteEntity
import com.daly.notes.feature_note.data.mappers.toNoteModel
import com.daly.notes.feature_note.data.mappers.toNoteModels
import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.repositories.NoteRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class NoteRepositoryImpl(
    private val dao: NoteDao
) : NoteRepository {

    override fun getNotes(): Flow<List<NoteModel>> {
        return dao.getNotes().map { it.toNoteModels() }
    }

    override suspend fun getNoteById(id: Int): NoteModel? {
        return dao.getNoteById(id)?.toNoteModel()
    }

    override suspend fun insertNote(note: NoteModel) {
        return dao.insertNote(note.toNoteEntity())
    }

    override suspend fun deleteNote(note: NoteModel) {
        return dao.deleteNote(note.toNoteEntity())
    }
}
