package com.daly.notes.feature_note.domain.use_cases

import com.daly.notes.feature_note.domain.models.NoteModel
import com.daly.notes.feature_note.domain.repositories.NoteRepository

class DeleteNoteUseCase(private val noteRepository: NoteRepository) {

    suspend operator fun invoke(note: NoteModel) {
        noteRepository.deleteNote(note)
    }
}
