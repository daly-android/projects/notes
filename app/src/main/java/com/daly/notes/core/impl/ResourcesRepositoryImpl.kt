package com.daly.notes.core.impl

import android.content.Context
import androidx.annotation.StringRes
import com.daly.notes.core.ResourcesRepository

/**
 * [ResourcesRepository] implementation for framework usage
 * @param appContext injected to an android appContext to fetch resources from XML file
 */
class ResourcesRepositoryImpl(private val appContext: Context) : ResourcesRepository {

    /**
     * Fetch a string in string.xml
     * @param id The identifier of the string resources
     * @param args An undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchString(@StringRes id: Int, vararg args: Any?): String = appContext.getString(id, *args)
}
