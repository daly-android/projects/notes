package com.daly.notes.core

import kotlin.coroutines.CoroutineContext

interface DispatcherService {

    val io: CoroutineContext

    val main: CoroutineContext

    val default: CoroutineContext

    val unconfined: CoroutineContext
}
