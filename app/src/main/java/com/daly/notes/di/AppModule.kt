package com.daly.notes.di

import android.app.Application
import androidx.room.Room
import com.daly.notes.core.DispatcherService
import com.daly.notes.core.ResourcesRepository
import com.daly.notes.core.impl.DispatcherServiceImpl
import com.daly.notes.core.impl.ResourcesRepositoryImpl
import com.daly.notes.feature_note.data.data_source.NoteDatabase
import com.daly.notes.feature_note.data.data_source.NoteDatabase.Companion.DATABASE_NAME
import com.daly.notes.feature_note.data.repositories.NoteRepositoryImpl
import com.daly.notes.feature_note.domain.repositories.NoteRepository
import com.daly.notes.feature_note.domain.use_cases.AddNoteUseCase
import com.daly.notes.feature_note.domain.use_cases.DeleteNoteUseCase
import com.daly.notes.feature_note.domain.use_cases.GetNoteUseCase
import com.daly.notes.feature_note.domain.use_cases.GetNotesUseCase
import com.daly.notes.feature_note.domain.use_cases.NoteUseCases
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideNoteDatabase(app: Application): NoteDatabase {
        return Room.databaseBuilder(
            context = app,
            klass = NoteDatabase::class.java,
            name = DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideNoteRepository(db: NoteDatabase): NoteRepository {
        return NoteRepositoryImpl(db.noteDao)
    }

    @Provides
    @Singleton
    fun provideNoteUseCases(repository: NoteRepository): NoteUseCases {
        return NoteUseCases(
            getNotes = GetNotesUseCase(repository),
            deleteNote = DeleteNoteUseCase(repository),
            addNote = AddNoteUseCase(repository),
            getNote = GetNoteUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideResourcesRepository(app: Application): ResourcesRepository {
        return ResourcesRepositoryImpl(app)
    }

    @Provides
    fun provideDispatcherProvider(): DispatcherService {
        return DispatcherServiceImpl()
    }
}
